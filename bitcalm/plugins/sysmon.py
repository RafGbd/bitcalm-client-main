import time
import Queue as queue

from bitcalm.core.plugins import SingleThreadPlugin
from bitcalm.core.tasks import MemNotifyTask


class MemQueue(queue.Queue):
    def __init__(self, *args, **kwargs):
        queue.Queue.__init__(self, *args, **kwargs)
        self._amount_min = 0
        self._amount_max = 0
        self._item_max = None

    def put(self, item, *args, **kwargs):
        queue.Queue.put(self, item, *args, **kwargs)
        if item.amount < self.amount_min:
            self._amount_min = item.amount
        if item.amount > self.amount_max:
            self._amount_max = item.amount

    def get(self, max_amount=None, block=True, timeout=None):
        self._item_max = max_amount
        if max_amount:
            if not self._qsize() or max_amount < self.amount_min:
                return None
            block = False
        item = queue.Queue.get(self, block=block, timeout=timeout)
        if item is not None:
            if self._qsize():
                if item.amount == self._amount_min:
                    self._amount_min = None
                elif item.amount == self._amount_max:
                    self._amount_max = None
            else:
                self._amount_min = self._amount_max = 0
        return item

    def _get(self):
        if self._item_max:
            for item in self.queue:
                if item.amount <= self._item_max:
                    break
            else:
                return None
            self.queue.remove(item)
            return item
        return queue.Queue._get(self)

    @property
    def amount_min(self):
        if self._amount_min is None:
            self._amount_min = min(self.queue).amount if self._qsize() else 0
        return self._amount_min

    @property
    def amount_max(self):
        if self._amount_max is None:
            self._amount_max = max(self.queue).amount if self._qsize() else 0
        return self._amount_max


class SysMonitor(SingleThreadPlugin):
    def __init__(self, *args, **kwargs):
        SingleThreadPlugin.__init__(self, *args, **kwargs)
        self.core.register_service(MemNotifyTask, self)
        self.queue = MemQueue()

    def add_task(self, task):
        self.queue.put(task)

    def loop(self):
        while not self._stop.is_set():
            while self.queue.empty():
                time.sleep(1)
            available = self.core.get_available_memory()
            while available >= self.queue.amount_min:
                task = self.queue.get(max_amount=available)
                available -= task.amount
                task.on_complete()
            time.sleep(1)


plugins = [SysMonitor]
