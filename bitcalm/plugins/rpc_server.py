import logging
import os
import os.path
import SocketServer
import tempfile
import Queue as queue

from collections import defaultdict

from SimpleXMLRPCServer import SimpleXMLRPCServer
from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler

from bitcalm.core.plugins import SingleThreadPlugin
from bitcalm.core.tasks import CompressTask


class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

    def __init__(self, request, client_address, server):
        client_address = ("localhost", 0)
        SimpleXMLRPCRequestHandler.__init__(self, request, client_address, server)

    def setup(self):
        self.connection = self.request
        if self.timeout is not None:
            self.connection.settimeout(self.timeout)
        # if self.disable_nagle_algorithm:
        #     self.connection.setsockopt(socket.IPPROTO_TCP,
        #                                socket.TCP_NODELAY, True)
        self.rfile = self.connection.makefile('rb', self.rbufsize)
        self.wfile = self.connection.makefile('wb', self.wbufsize)


class XMLRPCServer(SocketServer.UnixStreamServer, SimpleXMLRPCServer):
    def __init__(self, addr, requestHandler=RequestHandler,
                 logRequests=1):

        if os.access(addr, 0):
            os.remove(addr)

        self.funcs = {}
        self.logRequests = logRequests
        self.instance = None
        self.encoding = 'utf-8'
        self.allow_none = True
        SocketServer.UnixStreamServer.__init__(self, addr, requestHandler)


class RPCServer(SingleThreadPlugin):
    def __init__(self, core, *args, **kwargs):
        super(RPCServer, self).__init__(core, *args, **kwargs)
        self.core = core
        self.queue = queue.Queue()
        self.tasks = defaultdict(set)

        default_server_addr = os.path.abspath('bitcalm.sock')
        server_addr = core.config.get('rpc', 'sock_file', default_server_addr)

        self.server = XMLRPCServer(server_addr)

        self.server.register_function(self.core.status, 'status')
        self.server.register_function(self.core.stop, 'stop')
        self.server.register_function(self.core.start, 'start')
        self.server.register_function(self.core.restart, 'restart')
        self.server.register_function(self.core.load_plugins, 'load')
        self.server.register_function(self.core.reload_plugin, 'reload')
        self.server.register_function(self.core.get_backup_status, 'backup_status')


    def add_task(self, task):
        if task.input not in self.tasks:
            self.tasks[task.input].add(task)
            self.queue.put(task.input)
            task.on_receive()

    def loop(self):
        logging.debug("starting RPCServer plugin")
        self.status = 'working'
        self.server.serve_forever()
            


plugins = [RPCServer]
