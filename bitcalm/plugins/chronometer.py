import os
from datetime import datetime
from collections import defaultdict

from bitcalm.core import jsonstorage
from bitcalm.core.const import DATA_DIR
from bitcalm.core.plugins import EventPlugin
from bitcalm.core.events import TaskFinishedEvent


class LastModDict(defaultdict):
    def __init__(self, *args, **kwargs):
        defaultdict.__init__(self, *args, **kwargs)
        self.last_modified = datetime.utcnow()

    def __setitem__(self, key, value):
        defaultdict.__setitem__(self, key, value)
        self.on_modified()

    def __delitem__(self, key):
        defaultdict.__delitem__(self, key)
        self.on_modified()

    def on_modified(self):
        self.last_modified = datetime.utcnow()


class FileDict(LastModDict):
    def __init__(self, path, *args, **kwargs):
        LastModDict.__init__(self, *args, **kwargs)
        self.path = path
        self.last_saved = None

    def is_modified(self):
        if not self.last_saved:
            return True
        return self.last_saved < self.last_modified

    def save(self):
        jsonstorage.save(self.path, self)
        self.last_saved = datetime.utcnow()


class Chronometer(EventPlugin):
    def __init__(self, *args, **kwargs):
        EventPlugin.__init__(self, *args, **kwargs)
        self.core.add_listener(TaskFinishedEvent, self)
        self.durations = FileDict(os.path.join(DATA_DIR, 'chronometer.data'),
                                  float)

    def process_event(self, event):
        if isinstance(event, TaskFinishedEvent):
            self.durations[type(event.task).__name__] += event.task.duration
            self.durations.save()


plugins = [Chronometer]
