import time
import threading
import Queue as queue
from datetime import datetime

from bitcalm.core.plugins import TaskPlugin, PluginData
from bitcalm.core.tasks import BackupStatusTask
from bitcalm.core.events import BackupEvent
from bitcalm.utils import total_seconds
from bitcalm.core.backup import Backup
from bitcalm.core.const import DAY


class BackupController(TaskPlugin):
    def __init__(self, *args, **kwargs):
        TaskPlugin.__init__(self, *args, **kwargs)
        self.backup_period = DAY
        self.backup = None
        self.backup_thread = None
        self.tasks = queue.Queue()
        self.data = PluginData('backupctl')  # TODO: add filename to the config
        self.data.load()
        self.core.register_service(BackupStatusTask, self)

    def add_task(self, task):
        self.tasks.put(task)
        task.on_receive()

    def backup_ctl(self):
        self.backup = Backup()
        self.backup.on_start()
        self.data['current_backup_start'] = self.backup.time.start_ts
        self.data.save()
        self.core.notify(BackupEvent(self.backup))
        self.backup.wait()
        prev_backup = self.data.get('current_backup_start')
        self.data['prev_backup'] = prev_backup
        # TODO: move backup period to the config
        self.data['next_backup'] = prev_backup + DAY
        del self.data['current_backup_start']
        self.data.save()
        self.backup.on_finish()

    def process_task(self, task):
        task.on_start()
        # There is only status tasks now, so we do not check task type.
        if self.backup is None:
            task.result = {}
            for k in ('prev_backup', 'next_backup'):
                try:
                    task.result[k] = datetime.utcfromtimestamp(self.data[k])
                except KeyError:
                    task.result[k] = None
        else:
            task.result = self.backup.get_status()
        task.on_complete()

    def delay_backup(self, when):
        if isinstance(when, datetime):
            now = datetime.utcnow()
            delay = total_seconds(when - now) if when > now else 0
            self.data['next_backup'] = time.mktime(when.utctimetuple())
        else:
            delay = max(0, time.time() - when)
            self.data['next_backup'] = when
        if self.backup_thread and self.backup_thread.is_alive():
            self.backup_thread.cancel()
            if self.backup_thread.is_alive():
                return  # TODO: backup is running, raise exception
        self.backup_thread = threading.Timer(delay, self.backup_ctl)
        self.backup_thread.name = 'Backup controller'
        self.backup_thread.setDaemon(True)
        self.backup_thread.start()

    def run(self):
        self.delay_backup(self.data.get('next_backup') or time.time())
        TaskPlugin.run(self)


plugins = [BackupController]
