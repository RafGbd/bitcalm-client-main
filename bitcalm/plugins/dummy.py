import logging
import time
import random
import Queue as queue
from cStringIO import StringIO
from collections import defaultdict

from bitcalm.core.plugins import SingleThreadPlugin
from bitcalm.core.tasks import CompressTask


class Dummy(SingleThreadPlugin):
    def __init__(self, *args, **kwargs):
        super(Dummy, self).__init__(*args, **kwargs)
        self.queue = queue.Queue()
        self.tasks = defaultdict(set)

    def add_task(self, task):
        if task.input not in self.tasks:
            self.tasks[task.input].add(task)
            self.queue.put(task.input)
            task.on_receive()

    def loop(self):
        self.status = 'waiting'
        logging.debug("starting Dummy loop")
        while not self._stop.is_set():
            logging.debug("inside Dummy loop")
            if random.randint(1, 6) == 1:
                raise IOError
            time.sleep(1)
            


plugins = [Dummy]
