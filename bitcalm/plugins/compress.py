import os
import logging
import gzip
import shutil
import Queue as queue
from cStringIO import StringIO
from collections import defaultdict

from bitcalm.core.plugins import SingleThreadPlugin
from bitcalm.utils import get_fileobj_length
from bitcalm.core.tasks import CompressTask
from bitcalm.core.const import CHUNK_SIZE


class GZipPipe(object):
    def __init__(self, source, close_src=False):
        self.source = source
        self.close_src = close_src
        self._compressed = StringIO()
        self._overage = StringIO()
        self._gz = gzip.GzipFile(fileobj=self._compressed, mode='wb')
        self._compressed.seek(0)
        self.name = getattr(source, 'name', None)
        self._fileobjects = (self._gz, self._compressed)
        self._closed = False
        self._eof = False
        self.seek = self._compressed.seek
        self.tell = self._compressed.tell

    def clean(self):
        if self._compressed.tell() >= get_fileobj_length(self._compressed):
            return
        shutil.copyfileobj(self._compressed, self._overage)
        self._compressed.truncate(0)
        self._overage.seek(0)
        shutil.copyfileobj(self._overage, self._compressed)
        self._overage.truncate(0)
        self._compressed.seek(0)

    def close(self):
        for fileobj in self._fileobjects:
            fileobj.close()
        if self.close_src:
            self.source.close()

    @property
    def closed(self):
        if not self._closed:
            self._closed = all((f.closed for f in self._fileobjects))
        return self._closed

    def read(self, size=-1):
        if not self._eof and size != 0:
            pos = self._compressed.tell()
            self._compressed.seek(0, os.SEEK_END)
            if size < 0:
                shutil.copyfileobj(self.source, self._gz)
                self._eof = True
            else:
                while self._compressed.tell() - pos < size:
                    # TODO: copy by chunks
                    data = self.source.read(size)
                    self._gz.write(data)
                    if len(data) < size:
                        self._eof = True
                        break
                    else:
                        self._gz.flush()
                    del data
            if self._eof:
                self._gz.close()
                if self.close_src:
                    self.source.close()
            self._compressed.seek(pos)
        return self._compressed.read(size)


class TGZipPipe(object):
    def __init__(self, source):
        self.source = source
        self._src_pos = source.tell()
        self._source_length = get_fileobj_length(source)
        self._main_buffer = StringIO()
        self._overage_buffer = StringIO()
        self._gzipfile = None
        self.name = getattr(source, 'name', None)
        self._fileobjects = (self._gz,
                             self.source,
                             self._main_buffer,
                             self._overage_buffer)

    @property
    def _gz(self):
        if self._gzipfile is None or self._gzipfile.closed:
            self._main_buffer.truncate(0)
            self._gzipfile = gzip.GzipFile(fileobj=self._main_buffer,
                                           mode='wb')
        return self._gzipfile

    @property
    def closed(self):
        return all((f.closed for f in self._fileobjects))

    def close(self):
        for fileobj in self._fileobjects:
            if not fileobj.closed:
                fileobj.close()

    def read(self, size=-1):
        if size < 0:
            self.source.seek(self._src_pos)
            shutil.copyfileobj(self.source, self._gz)
            self._gz.close()
            data = self._main_buffer.getvalue()
            self._main_buffer.truncate(0)
            return data
        if size == 0:
            return self._main_buffer.read(0)
        while self._main_buffer.tell() < size:
            data = self.source.read(size)
            if not data:
                break
            self._gz.write(data)
            if self.source.tell() < self._source_length:
                self._gz.flush()
            else:
                self._gz.close()
                break
        self._main_buffer.seek(0)
        data = self._main_buffer.read(size)
        shutil.copyfileobj(self._main_buffer, self._overage_buffer)
        self._main_buffer.truncate(0)
        self._main_buffer, self._overage_buffer = \
            self._overage_buffer, self._main_buffer
        return data


def compress(fileobj, gzipfile=None, rewind=True):
    if gzipfile is None:
        c= StringIO()
        gzipfile = gzip.GzipFile(fileobj=c, mode='wb')
        compress(fileobj, gzipfile=gzipfile)
        gzipfile.close()
        c.seek(0)
        return c
    if rewind:
        fileobj.seek(0)
    shutil.copyfileobj(fileobj, gzipfile)


def compress_chunks(chunks, chunk_size=CHUNK_SIZE):
    chunk = StringIO()
    gz = gzip.GzipFile(fileobj=chunk, mode='wb')
    for c in chunks:
        gz.write(c.read())
        gz.flush()
        if chunk.tell() > chunk_size:
            chunk.seek(0)
            yield chunk
            chunk.seek(0)
            chunk.truncate()
    gz.close()
    chunk.seek(0)
    yield chunk


class Compress(SingleThreadPlugin):
    def __init__(self, *args, **kwargs):
        SingleThreadPlugin.__init__(self, *args, **kwargs)
        self.core.register_service(CompressTask, self)
        self.queue = queue.Queue()
        self.tasks = defaultdict(set)

    def add_task(self, task):
        self.queue.put(task)
        task.on_receive()

    def compress(self, input, gzipfile=None):
        if isinstance(input, basestring):
            return GZipPipe(open(input, 'rb'), close_src=True)
        return GZipPipe(input)

    def loop(self):
        logging.debug("starting Compress loop")
        while not self._stop.is_set():
            try:
                task = self.queue.get(True, 1)
            except queue.Empty:
                continue
            task.on_start()
            try:
                compressed = self.compress(task.input, gzipfile=task.gzipfile)
            except:
                task.on_error()
            else:
                task.result = compressed
                task.on_complete()


plugins = [Compress]
