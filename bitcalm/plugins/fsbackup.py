import os
import re
import gzip
import stat
import shutil
import threading
import itertools
import Queue as queue
from cStringIO import StringIO

from bitcalm.core.events import BackupEvent
from bitcalm.core.plugins import BackupWorkerPlugin
from bitcalm.core.tasks import CompressTask, StoreTask, ReturnKeyMakerTask
from bitcalm.core.const import CHUNK_SIZE
from bitcalm.utils import chunks
from bitcalm.core.const import DATA_DIR
from bitcalm.core import jsonstorage


COMPRESSED = ('gz', 'bz', 'bz2', 'z', 'lzma', 'gzip', 'lz', 'lzo', 'xz',
              '7z', 'zip', 'tbz', 'tbz2', 'tgz', 'rar', 'sfx', 'bzip', 'bzip2',
              'deb', 'rpm', 'mint', 'pet',
              'lzm', 'ar',
              'jpg', 'jpeg', 'gif', 'png',
              'mp3', 'ogg')
COMPRESSED_PARTS = (r'7z\.\d{3}', r'r\d{2}', r'z\d{2}')
COMPRESSED_RE = re.compile(r'.*\.(?:%s)$' \
                                % '|'.join(COMPRESSED + COMPRESSED_PARTS))
IGNORE_DIRS = set(('dev', 'tmp', 'run'))


def is_compressed(path):
    return bool(COMPRESSED_RE.match(path))


def skip(path):
    try:
        mode = os.stat(path).st_mode
    except OSError:
        return True
    for func in (stat.S_ISLNK,
                 stat.S_ISCHR,
                 stat.S_ISBLK,
                 stat.S_ISFIFO,
                 stat.S_ISSOCK):
        if func(mode):
            return True
    return False


def separate_paths(paths):
    files = []
    dirs = []
    for path in paths:
        if os.path.isfile(path):
            files.append(path)
        elif os.path.isdir(path):
            dirs.append(path)
    return dirs, files


def iterfiles(path):
    for path, dirs, files in os.walk(path):
        for filename in files:
            yield  os.path.join(path, filename)


def get_root_items():
    for name in os.listdir('/'):
        if name in IGNORE_DIRS:
            continue
        yield '/' + name


def backupfiles(paths):
    dirs, files = separate_paths(paths)
    dirs = set(dirs)
    if '/' in dirs:
        dirs.remove('/')
        for name in get_root_items():
            dirs.add(name)
    while files:
        yield files.pop()
    for path in itertools.chain(*(iterfiles(d) for d in dirs)):
        if not skip(path):
            yield path


class ChunkCompress(object):
    def __init__(self, source, core, chunk_size=CHUNK_SIZE):
        self.source = source
        self.core = core
        self.chunks = chunks(source)
        self.chunk_size = chunk_size
        self.last_chunk = StringIO()
        self._gz = gzip.GzipFile(fileobj=self.last_chunk, mode='wb')
        self.count = 0
        self._compress()

    def is_finished(self):
        return self._gz.closed

    def _compress(self):
        try:
            while self.last_chunk.tell() < self.chunk_size:
                fevent = threading.Event()
                task = CompressTask(self.chunks.next(), gzipfile=self._gz,
                                    finished_event=fevent)
                self.core.add_task(task)
                fevent.wait()
                if task.is_error():
                    # TODO: change exception class
                    raise Exception('Compress error')
                shutil.copyfileobj(task.result, self.last_chunk)
        except StopIteration:
            self._gz.close()
        self.last_chunk.seek(0)
        self.count += 1
        return self.last_chunk

    def __iter__(self):
        yield self.last_chunk
        while not self._gz.closed:
            self.last_chunk.seek(0)
            self.last_chunk.truncate()
            yield self._compress()


class BackupHandler(object):
    def __init__(self, core, backup, paths, key_maker, stop_event,
                 backup_threads=3):
        self.core = core
        self.paths = paths
        self.keys = key_maker
        self.stop_event = stop_event
        self.backup_threads = backup_threads
        self.queue = queue.Queue(maxsize=2 * backup_threads)
        self.fsiterator = None
        self.backup = backup

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            pass  # TODO: set backup failed
        # TODO: update statistic
        # TODO: upload database
        # TODO: cancel pending tasks

    def make_key(self, path):
        return self.keys.make_key(path)

    def iterfiles(self):
        for path in backupfiles(self.paths):
            self.queue.put(path)

    def get_status(self):
        return self.backup.get_plugin_status('fsbackup')

    def update_status(self, *increase, **kwargs):
        with self.get_status() as status:
            if increase:
                for key in increase:
                    status[key] += 1
            if kwargs:
                for key, value in kwargs.iteritems():
                    if isinstance(value, int):
                        status[key] += value
                    else:
                        status[key] = value

    def backup_worker(self):
        task_finished = threading.Event()
        while not self.stop_event.is_set():
            try:
                path = self.queue.get(block=True, timeout=1)
            except queue.Empty:
                if self.fsiterator.is_alive():
                    continue
                else:
                    break
            if is_compressed(path):
                source = path
            else:
                task = CompressTask(input=path,
                                    finished_event=task_finished)
                self.core.add_task(task)
                task_finished.wait()
                task_finished.clear()
                if task.is_error():
                    continue  # TODO: log error
                source = task.result
            task = StoreTask(source, self.make_key(path),
                             finished_event=task_finished)
            self.core.add_task(task)
            task_finished.wait()
            task_finished.clear()
            if not getattr(source, 'closed', True):
                source.close()
            self.queue.task_done()
            if task.is_error():
                self.update_status('errors', last_error=path)
            else:
                self.update_status('complete', last_complete=path)
        self.update_status('workers_done')

    def make_backup(self):
        self.fsiterator = threading.Thread(target=self.iterfiles)
        self.fsiterator.setDaemon(True)
        self.fsiterator.start()
        workers = []
        for x in xrange(self.backup_threads):
            t = threading.Thread(target=self.backup_worker)
            t.setDaemon(True)
            t.start()
            workers.append(t)
        self.fsiterator.join()
        for t in workers:
            t.join()


class FSBackup(BackupWorkerPlugin):
    def __init__(self, *args, **kwargs):
        BackupWorkerPlugin.__init__(self, *args, **kwargs)
        config = jsonstorage.load(os.path.join(DATA_DIR, 'fsbackup.conf'))
        self.paths = config['backup']

    def process_event(self, event):
        if isinstance(event, BackupEvent):
            self.run_as_thread(self.backup, args=(event.backup,))

    def backup(self, backup):
        with backup:
            task = ReturnKeyMakerTask(backup_id=backup.id,
                                      plugin_name='fsbackup')
            self.core.add_task(task)
            task.wait()
            # TODO: check if task was failed
            args = (self.core, backup, self.paths, task.result, self._stop)
            with BackupHandler(*args) as handler:
                handler.make_backup()
            self.on_backup_complete()


plugins = [FSBackup]
