import logging
import os
from collections import defaultdict
from hashlib import sha384 as sha

from boto.s3.connection import S3Connection
from boto.s3.key import Key

from bitcalm.core.plugins import TaskPlugin
from bitcalm.core.tasks import StoreTask, ReturnKeyMakerTask
from bitcalm.utils import pipechunks as chunks, KeyMaker
from bitcalm.core.const import CONF_DIR, CHUNK_SIZE
from bitcalm.core import jsonstorage


def get_bucket(bucket_name, validate=True, **auth_data):
    conn = S3Connection(**auth_data)
    return conn.get_bucket(bucket_name, validate=validate)


class S3KeyMaker(KeyMaker):
    def __init__(self, username, *args, **kwargs):
        KeyMaker.__init__(self, *args, **kwargs)
        self.username = username
        self.template = os.path.join(username, self.template, '{filename}')

    def make_key(self, path):
        path = sha(path).hexdigest()
        return self.template.format(filename=path)


class TaskSet(object):
    def __init__(self, tasks):
        self.tasks = tasks

    def execute(self, name):
        for task in self.tasks:
            getattr(task, name)()

    def on_start(self):
        self.execute('on_start')

    def on_error(self):
        self.execute('on_error')

    def on_complete(self):
        self.execute('on_complete')


class S3Storage(TaskPlugin):
    def __init__(self, *args, **kwargs):
        TaskPlugin.__init__(self, *args, **kwargs)
        self.receivers = {StoreTask: self._add_store_task,
                          ReturnKeyMakerTask: self._add_task}
        for task_type in self.receivers.iterkeys():
            self.core.register_service(task_type, self)
        self.auth_data = jsonstorage.load(os.path.join(CONF_DIR,
                                                       's3storage.conf'))
        self.username = self.auth_data.pop('username')
        self.store_tasks = defaultdict(set)
        self.bucket = get_bucket(**self.auth_data)

    def _add_store_task(self, task):
        if task.ticket not in self.store_tasks:
            self.tasks.put(task)
        self.store_tasks[task.ticket].add(task)

    def _add_task(self, task):
        self.tasks.put(task)

    def add_task(self, task):
        for task_type, receive in self.receivers.iteritems():
            if isinstance(task, task_type):
                receive(task)
                task.on_receive()
                return
        task.on_reject()

    def _upload(self, source, dest):
        key = Key(self.bucket, name=dest)
        key.set_contents_from_file(source, encrypt_key=True, rewind=True)
        return key.size

    def _upload_mp(self, parts, dest):
        mp = self.bucket.initiate_multipart_upload(key_name=dest,
                                                   encrypt_key=True)
        size = 0
        try:
            for num, part in enumerate(parts, start=1):
                size += mp.upload_part_from_file(part, part_num=num).size
        except Exception:
            mp.cancel_upload()
            return 0
        mp.complete_upload()
        return size

    def _get_size(self, source):
        """
        :param source: string or seekable file-like object
        :return: size of the data
        """
        if isinstance(source, basestring):
            return os.stat(source).st_size
        try:
            return os.stat(source.name).st_size
        except:
            pass
        pos = source.tell()
        source.seek(0, os.SEEK_END)
        size = source.tell()
        source.seek(pos, os.SEEK_SET)
        return size

    def is_multipart_upload(self, source, chunk_size=CHUNK_SIZE):
        return self._get_size(source) > chunk_size

    def is_filelike(self, source):
        return hasattr(source, 'read') and callable(source.read)

    def upload(self, source, dest):
        if isinstance(source, basestring):
            with open(source, 'rb') as f:
                return self.upload(f, dest)
        if self.is_filelike(source) or isinstance(source, basestring):
            if self.is_multipart_upload(source):
                return self._upload_mp(chunks(source), dest)
            return self._upload(source, dest)
        return self._upload_mp(source, dest)

    def process_task(self, task):
        if isinstance(task, StoreTask):
            tasks = self.store_tasks[task.ticket]
            source, dest = task.source, task.dest
            if len(tasks) > 1:
                task = TaskSet(tasks)
            task.on_start()
            self.upload(source, dest)
        elif isinstance(task, ReturnKeyMakerTask):
            task.result = S3KeyMaker(backup_id=task.backup_id,
                                     plugin_name=task.plugin_name,
                                     username=self.username)
        task.on_complete()

    def loop(self):
        logging.debug("starting S3Storage loop")
        TaskPlugin.loop(self)

    def run(self):
        logging.info("starting s3storage plugin")
        TaskPlugin.run(self)


plugins = [S3Storage]
