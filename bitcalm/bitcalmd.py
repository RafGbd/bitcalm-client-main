#!/usr/bin/env python
import logging
import sys
import signal
from logging.handlers import RotatingFileHandler
from bitcalm.core.base import Core
from bitcalm.core.args_controller import parser, subparsers, opts_parser
from bitcalm.core.config import Config

def make_stopper(core):
    def stop_signal(signum, frame):
        logging.info("Stopping with siganl: %s" % signum)
        sys.exit(0)
    return stop_signal

def setup_logger(config):
    class StreamToLogger(object):
       """
       Fake file-like stream object that redirects writes to a logger instance.
       """
       def __init__(self, logger, log_level=logging.INFO):
          self.logger = logger
          self.log_level = log_level
          self.linebuf = ''
     
       def write(self, buf):
          for line in buf.rstrip().splitlines():
             self.logger.log(self.log_level, line.rstrip())

    log_file = config.get('logging', 'log_file', 'bitcalm.log')
    root_logger = logging.getLogger()
    sl = StreamToLogger(root_logger, logging.ERROR)
    sys.stderr = sl

    root_logger.setLevel("DEBUG")
    template = "%(asctime)s [%(filename)s:%(lineno)d %(threadName)s] [%(levelname)s]  %(message)s"
    log_formatter = logging.Formatter(template)
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)
    file_handler = RotatingFileHandler(log_file, maxBytes=25*1024*1024, backupCount=3)
    file_handler.setFormatter(log_formatter)
    root_logger.addHandler(file_handler)

def start_process(args):
    config = Config(args.config)
    setup_logger(config)
    logging.info("initializing bitcalm core...")
    core = Core(config)

    stop_signal = make_stopper(core)
    signal.signal(signal.SIGINT, stop_signal)
    signal.signal(signal.SIGHUP, stop_signal)
    signal.signal(signal.SIGTERM, stop_signal)

    logging.info("loading bitcalm plugins...")
    core.load_plugins()
    logging.info("starting plugins")
    core.run()
    logging.info("finishing...")

def main():
    run_parser = subparsers.add_parser('start_process', parents=[opts_parser])
    run_parser.set_defaults(func=start_process)
    parser.set_default_subparser('start_process')
    cmd = parser.parse_args()
    cmd.func(cmd)

if __name__ == '__main__':
    main()
