import os
import ConfigParser

class Config(object):
    def __init__(self, config_file):
        self.config_parser = ConfigParser.SafeConfigParser()
        self.config_parser.readfp(open(config_file))

    def get(self, section, option, default='__not_passed'):
        try:
            return self.config_parser.get(section, option)
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError), e:
            if default != '__not_passed':
                return default
            else:
                raise e
