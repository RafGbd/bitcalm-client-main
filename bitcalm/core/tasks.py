import threading
from datetime import datetime

from bitcalm.utils import total_seconds


class Task(object):
    class STATUS:
        CREATED = 0
        RECEIVED = 1
        STARTED = 2
        COMPLETE = 3
        ERROR = -1
        REJECTED = -2

    def __init__(self,
                 complete_queue=None,
                 finished_event=None,
                 error_queue=None,
                 drop_errors=False,
                 core=None):
        self.status = self.STATUS.CREATED
        self.complete_queue = complete_queue
        self.finished_event = finished_event
        if drop_errors:
            self.error_queue = None
        else:
            self.error_queue = error_queue or complete_queue
        self.result = None
        self.when_started = None
        self.when_finished = None
        self._duration = 0.0
        self.core = core  # TODO: use global core object if local is None

    @property
    def duration(self):
        if not self._duration:
            if self.when_started:
                if self.when_finished:
                    self._duration = self.when_finished - self.when_started
                    self._duration = total_seconds(self._duration)
                else:
                    return total_seconds(datetime.utcnow() - self.when_started)
        return self._duration

    def is_complete(self):
        return self.status == self.STATUS.COMPLETE

    def is_error(self):
        return self.status < 0

    def is_finished(self):
        return self.is_complete() or self.is_error()

    def _on_finished(self, status, queue):
        self.when_finished = datetime.utcnow()
        self.status = status
        if queue:
            queue.put(self)
        if self.finished_event:
            self.finished_event.set()

    def on_receive(self):
        self.status = self.STATUS.RECEIVED

    def on_start(self):
        self.when_started = datetime.utcnow()
        self.status = self.STATUS.STARTED

    def on_complete(self):
        self._on_finished(self.STATUS.COMPLETE, self.complete_queue)

    def on_error(self):
        self._on_finished(self.STATUS.ERROR, self.error_queue)

    def on_reject(self):
        self._on_finished(self.STATUS.REJECTED, self.error_queue)

    def wait(self, timeout=None):
        if not self.is_complete():
            if self.finished_event is None:
                self.finished_event = threading.Event()
            self.finished_event.wait(timeout=timeout)


class StoreTask(Task):
    def __init__(self, source, dest, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.source = source
        self.dest = dest
        self.ticket = (self.source, self.dest)


class CompressTask(Task):
    def __init__(self, input, gzipfile=None, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.input = input
        self.gzipfile = gzipfile


class BackupStatusTask(Task):
    pass


class ReturnKeyMakerTask(Task):
    def __init__(self, backup_id, plugin_name, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.backup_id = backup_id
        self.plugin_name = plugin_name


class MemNotifyTask(Task):
    def __init__(self, amount, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.amount = amount

    def __str__(self):
        return '%i bytes requirement' % self.amount

    def __cmp__(self, other):
        return cmp(self.amount, other.amount)
