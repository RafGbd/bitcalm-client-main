import os
import re
import imp
import stat
import threading
import logging
import Queue as queue

from bitcalm.core.const import PLUGIN_STATUS
from bitcalm.core.events import BackupEvent
from bitcalm.collections import FileDict
from bitcalm.utils import run_as_thread
from bitcalm.core.const import DATA_DIR


PLUGIN_DIRS = [os.path.join(os.path.dirname(os.path.dirname(__file__)),
                            'plugins')]
PLUGIN_NAME_RE = re.compile('(\w+)\.py$')

        
class Plugin(object):
    restart_attempts = 0
    restart_countdown = 0

    def __init__(self, core):
        self.core = core
        self._stop = threading.Event()
        self.status = PLUGIN_STATUS.INITIALIZED

    def stop(self):
        self._stop.set()

    def status_wrapper(self, executable):
        def wrap(*args, **kwargs):
            try:
                self.status = PLUGIN_STATUS.INITIALIZED
                executable(*args, **kwargs)
            except Exception, e:
                self.status = PLUGIN_STATUS.FAILED
                self._error = repr(e)
                logging.exception(e)
            else:
                self.status = PLUGIN_STATUS.FINISHED
        return wrap

    def run_as_thread(self,
                      executable,
                      args=(),
                      kwargs=None,
                      daemon=True,
                      name=None):
        self._stop.clear()
        return run_as_thread(self.status_wrapper(executable),
                             args=args,
                             kwargs=kwargs,
                             daemon=daemon,
                             name=name)

    def is_alive(self):
        raise NotImplementedError

    def run(self):
        raise NotImplementedError


class SingleThreadPlugin(Plugin):
    def __init__(self, *args, **kwargs):
        super(SingleThreadPlugin, self).__init__(*args, **kwargs)
        self.thread = None

    def is_alive(self):
        return self.thread and self.thread.is_alive()

    def run(self):
        self.thread = self.run_as_thread(self.loop,
                                         name=type(self).__name__ + '-Loop')

    def loop(self):
        raise NotImplementedError


class LoopPlugin(Plugin):
    def __init__(self, *args, **kwargs):
        Plugin.__init__(self, *args, **kwargs)
        self.loop_thread = None

    def is_alive(self):
        return self.loop_thread and self.loop_thread.is_alive()

    def get_loop_name(self):
        return type(self).__name__ + '-Loop'

    def run(self):
        self.loop_thread = self.run_as_thread(self.loop,
                                              name=self.get_loop_name())

    def loop(self):
        raise NotImplementedError


class EventPlugin(LoopPlugin):
    def __init__(self, *args, **kwargs):
        LoopPlugin.__init__(self, *args, **kwargs)
        self.events = queue.Queue()

    def notify(self, event):
        self.events.put(event)

    def process_event(self, event):
        raise NotImplementedError

    def loop(self):
        while not self._stop.is_set():
            try:
                event = self.events.get(True, 1)
            except queue.Empty:
                continue
            self.process_event(event)
            self.events.task_done()


class TaskPlugin(LoopPlugin):
    def __init__(self, *args, **kwargs):
        LoopPlugin.__init__(self, *args, **kwargs)
        self.tasks = queue.Queue()

    def add_task(self, task):
        self.tasks.put(task)
        task.on_receive()

    def process_task(self, task):
        raise NotImplementedError

    def loop(self):
        while not self._stop.is_set():
            try:
                task = self.tasks.get(timeout=1)
            except queue.Empty:
                continue
            try:
                self.process_task(task)
            except:
                task.on_error()
            self.tasks.task_done()


class BackupPlugin(EventPlugin):
    def __init__(self, *args, **kwargs):
        EventPlugin.__init__(self, *args, **kwargs)
        self.core.add_listener(BackupEvent, self)


class BackupWorkerPlugin(BackupPlugin):
    def __init__(self, *args, **kwargs):
        BackupPlugin.__init__(self, *args, **kwargs)
        # Event, which is set to True when work for current backup is complete.
        self.backup_complete = None

    def notify(self, event):
        if isinstance(event, BackupEvent):
            name = type(self).__name__
            self.backup_complete = event.backup.register_worker(name)
        EventPlugin.notify(self, event)

    def on_backup_complete(self):
        self.backup_complete.set()


class PluginData(FileDict):
    def __init__(self, filename, path=DATA_DIR):
        path = os.path.join(path, filename)
        FileDict.__init__(self, path=path)


def load(dirs=PLUGIN_DIRS):
    plugins = []
    for name, path in find(dirs=dirs).iteritems():
        logging.debug('loading plugin %(name)s at %(path)s' % locals())
        m = imp.load_source(name, path)
        plugins.extend(m.plugins)
    return plugins


def find(dirs=PLUGIN_DIRS):
    plugins = {}
    for d in dirs:
        content = os.listdir(d)
        for item in content:
            name = PLUGIN_NAME_RE.match(item)
            if name:
                path = os.path.join(d, item)
                if stat.S_ISREG(os.stat(path).st_mode):
                    plugins[name.group(1)] = path
    if '__init__' in plugins:
        del plugins['__init__']
    return plugins
