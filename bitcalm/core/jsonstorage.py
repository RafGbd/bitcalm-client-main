import os
import json


def load(path, default=None):
    if not os.path.exists(path):
        return default
    with open(path) as f:
        try:
            return json.load(f)
        except ValueError:
            return default


def save(path, data):
    with open(path, 'w') as f:
        json.dump(data, f)
