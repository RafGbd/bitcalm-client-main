class Event(object):
    pass


class BackupEvent(Event):
    def __init__(self, backup):
        self.backup = backup


class TaskFinishedEvent(Event):
    def __init__(self, task):
        self.task = task
