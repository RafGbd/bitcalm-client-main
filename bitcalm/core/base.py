import logging
import time
import os
from collections import defaultdict, OrderedDict

from bitcalm.core.const import MAX_RESTART_ATTEMPTS
from bitcalm.core.plugins import load as load_plugins
from bitcalm.core.tasks import BackupStatusTask
from bitcalm.utils import get_memory_usage
from bitcalm.core.const import MB


class Core(object):
    def __init__(self, config):
        self.config = config
        self.plugins = OrderedDict()
        self.listeners = defaultdict(set)
        self.services = {}

    def load_plugins(self):
        result = {"success": True, "msg":''}
        plugins = load_plugins()
        failed_plugins = []
        for plugin in plugins:
            plugin_name = plugin.__name__.lower()
            if plugin_name not in self.plugins:
                try:
                    plugin.instance = plugin(self)
                    self.plugins[plugin_name] = plugin
                except Exception, e:
                    logging.exception(e)
                    failed_plugins.append(plugin_name)
        if failed_plugins:
            result["success"] = False
            result["msg"] = "can't load following plugins: %s" % (", ").join(failed_plugins)
        return result

    def reload_plugin(self, plugin_name):
        result = self.stop(plugin_name)
        if result["success"]:
            del self.plugins[plugin_name]
            plugins = load_plugins()
            for plugin in plugins:
                if plugin_name == plugin.__name__.lower():
                    try:
                        plugin.instance = plugin(self)
                        self.plugins[plugin_name] = plugin
                        result = self.start(plugin_name)
                    except Exception, e:
                        logging.exception(e)
                        result["success"] = False
                        result["msg"] = "failed to load plugin"
                    finally:
                        break
            else:
                result["success"] = False
                result["msg"] = "plugin hasn't found"
        return result

    def run(self):
        for plugin in self.plugins.values():
            plugin.instance.run()
        self.supervise()

    def stop(self, plugin_name):
        result = {"success": False, "msg": ''}
        plugin = self.plugins.get(plugin_name.lower())
        if plugin:
            plugin.instance.stop()
            plugin.instance.thread.join(5)
            if not plugin.instance.is_alive():
                result['success'] = True
            else:
                #somehow we need to terminate thread
                result['success'] = False
                result['msg'] = "plugin isn't responsing"
        else:
            result['success'] = False
            result['msg'] = "plugin hasn't found"
        return result 

    def start(self, plugin_name):
        result = {"success": False, "msg": ''}
        plugin = self.plugins.get(plugin_name.lower())
        if plugin:
            plugin.restart_attempts = 0
            plugin.instance.run()
            if plugin.instance.is_alive():
                result['success'] = True
        else:
            result['success'] = False
            result['msg'] = "plugin hasn't found"
        return result

    def restart(self, plugin_name):
        stop_result = self.stop(plugin_name)
        if stop_result['success']:
            start_result = self.start(plugin_name)
            return start_result
        return stop_result

    def status(self):
        result = []
        for plugin_name, plugin in self.plugins.items():
            plugin_status = {
                'name': plugin_name,
                'status': plugin.instance.status
            }
            result.append(plugin_status)
        return result

    def supervise(self):
        while True:
            for plugin_name, plugin in self.plugins.items():
                message_tpl = 'supervisor: plugin %(plugin_name)s is in "%(plugin_status)s" status'
                logging.debug(message_tpl % {
                    'plugin_name': plugin.__name__,
                    'plugin_status': plugin.instance.status
                })
                if plugin.instance.status == 'failed':
                    if plugin.restart_attempts <= MAX_RESTART_ATTEMPTS:
                        if plugin.restart_countdown == 0:
                            plugin.restart_attempts += 1
                            plugin.restart_countdown = plugin.restart_attempts * 2
                            logging.debug("supervisor: trying to restart %s plugin" % plugin_name)
                            plugin.instance.run()
                        else:
                            plugin.restart_countdown -= 1
            time.sleep(1)

    def add_task(self, task):
        service = self.services.get(type(task))
        if service:
            task.core = self
            return service.add_task(task)
        task.on_reject()
        return None

    def notify(self, event):
        for listener in self.listeners[type(event)]:
            listener.notify(event)

    def register_service(self, task, service):
        # TODO: check if there is a service for this task
        # and raise an exception if it is there.
        self.services[task] = service
        return True

    def add_listener(self, event, listener):
        self.listeners[event].add(listener)
        return True

    def get_available_memory(self):
        # TODO: move memory limit to config
        memlimit = 100 * MB
        return max(memlimit - get_memory_usage(), 0)

    def has_memory(self, amount):
        return amount <= self.get_available_memory()

    def get_backup_status(self):
        task = BackupStatusTask()
        self.add_task(task)
        task.wait()
        return task.result if task.is_complete() else 'error'
