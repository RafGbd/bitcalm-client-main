import argparse
import os
import sys

from bitcalm.core import rpc_client

def set_default_subparser(self, name, args=None):
    """default subparser selection. Call after setup, just before parse_args()
    name: is the name of the subparser to call by default
    args: if set is the argument list handed to parse_args()

    , tested with 2.7, 3.2, 3.3, 3.4
    it works with 2.6 assuming argparse is installed
    """
    subparser_found = False
    for arg in sys.argv[1:]:
        if arg in ['-h', '--help']:  # global help if no subparser
            break
    else:
        for x in self._subparsers._actions:
            if not isinstance(x, argparse._SubParsersAction):
                continue
            for sp_name in x._name_parser_map.keys():
                if sp_name in sys.argv[1:]:
                    subparser_found = True
        if not subparser_found:
            # insert default in first position, this implies no
            # global options without a sub_parsers specified
            if args is None:
                sys.argv.insert(1, name)
            else:
                args.insert(0, name)

argparse.ArgumentParser.set_default_subparser = set_default_subparser

opts_parser = argparse.ArgumentParser(add_help=False)
default_config_file = os.getenv('BITCALM_CONFIG', '/etc/bitcalm/bitcalm.conf')
opts_parser.add_argument('--config', nargs='?', const=default_config_file, type=str, default=default_config_file)

parser = argparse.ArgumentParser(parents=[opts_parser])
subparsers = parser.add_subparsers()

status_parser = subparsers.add_parser('status', help="show plugins' statuses",
    parents=[opts_parser])
status_parser.set_defaults(func=rpc_client.status)

status_parser = subparsers.add_parser('load', help="load new plugins \
    (already working plugins won't be affected)", parents=[opts_parser])
status_parser.set_defaults(func=rpc_client.load)

start_parser = subparsers.add_parser('start', help="start certain plugin",
    parents=[opts_parser])
start_parser.add_argument('plugin')
start_parser.set_defaults(func=rpc_client.start)

stop_parser = subparsers.add_parser('stop', help="stop certain plugin",
    parents=[opts_parser])
stop_parser.add_argument('plugin')
stop_parser.set_defaults(func=rpc_client.stop)

restart_parser = subparsers.add_parser('restart', help="restart certain plugin",
    parents=[opts_parser])
restart_parser.add_argument('plugin')
restart_parser.set_defaults(func=rpc_client.restart)

reload_parser = subparsers.add_parser('reload', help="reload certain plugin",
    parents=[opts_parser])
reload_parser.add_argument('plugin')
reload_parser.set_defaults(func=rpc_client.reload_plugin)

reload_parser = subparsers.add_parser('backup',
                                      help="show backup status",
                                      parents=[opts_parser])
reload_parser.add_argument('action')
reload_parser.set_defaults(func=rpc_client.backup)