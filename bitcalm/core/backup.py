import threading
from time import time
from datetime import datetime
from collections import defaultdict

from bitcalm.utils import Duration
from bitcalm.collections import LockDict


class BStat(LockDict):
    def __init__(self, default=int, *args, **kwargs):
        LockDict.__init__(self, *args, **kwargs)
        self.default = default
        self.last_update = datetime.utcnow()

    def _on_update(self):
        self.last_update = datetime.utcnow()

    def __getitem__(self, key):
        try:
            return LockDict.__getitem__(self, key)
        except KeyError:
            self[key] = value = self.default()
            return value

    def __setitem__(self, key, value):
        LockDict.__setitem__(self, key, value)
        self._on_update()

    def __delitem__(self, key):
        LockDict.__delitem__(self, key)
        self._on_update()


class Backup(object):
    def __init__(self, id=None, stat_cls=BStat):
        self.id = id or int(time())  # TODO: make unique id
        self._lock = threading.Lock()
        # Worker plugin is plugin performing backup.
        # We must wait for completion of this plugins' work
        # to mean that backup is finished.
        self.worker_plugins = {}
        self.status = defaultdict(stat_cls)
        self.time = Duration()

    def __enter__(self):
        self.lock()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()

    def register_worker(self, name, event=None):
        if event is None:
            event = threading.Event()
        self.worker_plugins[name] = event
        return event

    def on_start(self):
        self.time.start()

    def on_finish(self):
        self.time.end()

    def get_plugin_status(self, plugin_name):
        return self.status[plugin_name]

    def get_status(self):
        status = {'start': self.time.start_dt,
                  'end': self.time.end_dt,
                  'duration': self.time.duration}
        plugins = self.worker_plugins.values()
        plugins = {'done': sum((1 for e in plugins if e.is_set())),
                   'total': len(plugins),
                   'status': {k: dict(v) for k, v in self.status.items()}}
        status['plugins'] = plugins
        return status

    def lock(self):
        self._lock.acquire()

    def release(self):
        self._lock.release()

    def wait(self):
        for event in self.worker_plugins.values():
            event.wait()
