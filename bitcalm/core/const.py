CONF_DIR = '/etc/bitcalm'
DATA_DIR = '/var/lib/bitcalm'


KB = 1 << 10
MB = KB << 10

MICROSEC = 1 / 10.0 ** 6
MIN = 60
HOUR = 60 * MIN
DAY = 24 * HOUR


class PLUGIN_STATUS:
    INITIALIZED = 'initialized'
    FAILED = 'failed'
    WAITING = 'waiting'
    WORKING = 'working'
    FINISHED = 'finished'


MAX_RESTART_ATTEMPTS = 5


CHUNK_SIZE = 32*MB  # TODO: move it to the config
