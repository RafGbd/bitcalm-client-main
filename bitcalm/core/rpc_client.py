import argparse
import httplib
import os
import socket
import sys
import tempfile
import xmlrpclib

from bitcalm.core.config import Config

class UnixStreamHTTPConnection(httplib.HTTPConnection):
    def connect(self):
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.sock.connect(self.host)

class UnixStreamTransport(xmlrpclib.Transport, object):
    def __init__(self, socket_path):
        self.socket_path = socket_path
        super(UnixStreamTransport, self).__init__()

    def make_connection(self, host):
        return UnixStreamHTTPConnection(self.socket_path)


def get_rpc_server(config):
    default_server_addr = os.path.abspath('bitcalm.sock')
    server_addr = config.get('rpc', 'sock_file', default_server_addr)
    return xmlrpclib.ServerProxy('http://arg_unused',
                                 transport=UnixStreamTransport(server_addr))


def rpc_client_command(func):
    def wrap(args):
        serv = get_rpc_server(Config(args.config))
        try:
            func(serv, args)
        except Exception, e:
            sys.stdout.write('Error: %s\n' % e)
    return wrap

@rpc_client_command
def stop(serv, args):
    print serv.stop(args.plugin)

@rpc_client_command
def start(serv, args):
    print serv.start(args.plugin)

@rpc_client_command
def restart(serv, args):
    print serv.restart(args.plugin)

@rpc_client_command
def reload_plugin(serv, args):
    print serv.reload(args.plugin)

@rpc_client_command
def load(serv, args):
    print serv.load()

@rpc_client_command
def status(serv, args):
    plugin_status_list = serv.status()
    for plugin_status in plugin_status_list:
        sys.stdout.write('{name:20} {status}\n'.format(**plugin_status))

@rpc_client_command
def backup(serv, args):
    # TODO: check args.action, now it is always meaned as "status"
    print serv.backup_status()


parser = argparse.ArgumentParser()

subparsers = parser.add_subparsers()

status_parser = subparsers.add_parser('status', help="show plugins' statuses")
status_parser.set_defaults(func=status)

status_parser = subparsers.add_parser('load', help="load new plugins \
    (already working plugins won't be affected)")
status_parser.set_defaults(func=load)

start_parser = subparsers.add_parser('start', help="start certain plugin")
start_parser.add_argument('plugin')
start_parser.set_defaults(func=start)

stop_parser = subparsers.add_parser('stop', help="stop certain plugin")
stop_parser.add_argument('plugin')
stop_parser.set_defaults(func=stop)

restart_parser = subparsers.add_parser('restart', help="restart certain plugin")
restart_parser.add_argument('plugin')
restart_parser.set_defaults(func=restart)

reload_parser = subparsers.add_parser('reload', help="reload certain plugin")
reload_parser.add_argument('plugin')
reload_parser.set_defaults(func=reload_plugin)

reload_parser = subparsers.add_parser('backup', help="show backup status")
reload_parser.add_argument('action')
reload_parser.set_defaults(func=backup)

def handle_command():
    cmd = parser.parse_args()
    cmd.func(cmd)
