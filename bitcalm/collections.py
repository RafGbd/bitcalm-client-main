import threading
from bitcalm.core import jsonstorage


class LockDict(dict):
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self._lock = threading.Lock()

    def __enter__(self):
        self._lock.acquire()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._lock.release()


class FileDict(dict):
    def __init__(self, path, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self.path = path

    def save(self):
        jsonstorage.save(self.path, self)

    def load(self):
        self.clear()
        upd = jsonstorage.load(self.path)
        if upd:
            self.update(upd)
