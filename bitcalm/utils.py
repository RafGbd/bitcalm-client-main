import os
import re
import math
import threading
from time import time
from datetime import datetime

from bitcalm.core.const import MICROSEC, DAY, CHUNK_SIZE


PROC_STATUS = '/proc/%d/status' % os.getpid()
MEM_RE = re.compile('VmRSS:.+?(\d+)')


def total_seconds(td):
    return td.days * DAY + td.seconds + td.microseconds * MICROSEC


def get_memory_usage():
    proc_status = open(PROC_STATUS)
    try:
        status = proc_status.readlines()
    finally:
        proc_status.close()

    for line in status:
        m = MEM_RE.match(line)
        if m:
            return int(m.group(1)) << 10  # KB to bytes
    return None


def get_fileobj_length(fileobj):
    pos = fileobj.tell()
    fileobj.seek(0, os.SEEK_END)
    length = fileobj.tell()
    fileobj.seek(pos)
    return length


class FileChunk(object):
    def __init__(self, fileobj, length, offset=0):
        self.fileobj = fileobj
        self.offset = offset
        self.length = length
        self.end = offset + length
        self.fileobj.seek(self.offset)

    def seek(self, offset, whence=os.SEEK_SET):
        if whence == os.SEEK_SET:
            offset += self.offset
        elif whence == os.SEEK_CUR:
            offset += self.fileobj.tell()
        elif whence == os.SEEK_END:
            offset += self.end
        else:
            raise ValueError('Invalid whence: {}'.format(whence))
        if offset > self.end:
            offset = self.end
        elif offset < self.offset:
            offset = self.offset
        self.fileobj.seek(offset)

    def tell(self):
        return self.fileobj.tell() - self.offset

    def read(self, n=-1):
        if n != 0:
            count = self.end - self.fileobj.tell()
            n = count if n < 0 else min(n, count)
        return self.fileobj.read(n)


def chunks(source, chunk_size=CHUNK_SIZE):
    if isinstance(source, basestring):
        with open(source, 'r') as f:
            for chunk in chunks(f, chunk_size=chunk_size):
                yield chunk
    else:
        length = get_fileobj_length(source)
        total_chunks = int(math.ceil(length / float(chunk_size)))
        for i in xrange(total_chunks):
            offset = chunk_size * i
            yield FileChunk(source, chunk_size, offset=offset)


def pipechunks(source, chunk_size=CHUNK_SIZE):
    if isinstance(source, basestring):
        with open(source, 'rb') as f:
            for chunk in chunks(f, chunk_size=chunk_size):
                yield chunk
    else:
        offset = source.tell()
        chunk = FileChunk(source, chunk_size, offset=offset)
        yield chunk
        # chunk must be readen
        while source.tell() >= chunk.end:
            try:
                source.clean()
            except:
                offset += chunk_size
            chunk = FileChunk(source, chunk_size, offset=offset)
            yield chunk


def run_as_thread(executable, args=(), kwargs=None, daemon=True, name=None):
    t = threading.Thread(target=executable, args=args, kwargs=kwargs)
    t.setDaemon(daemon)
    if name:
        t.name = name
    t.start()
    return t


class Duration(object):
    def __init__(self):
        self.start_ts = None
        self.end_ts = None
        self._start_dt = None
        self._end_dt = None
        self._duration = None

    @property
    def duration(self):
        if self._duration is None:
            if self.start_ts is None:
                return 0
            if self.end_ts is None:
                return time() - self.start_ts
            self._duration = self.end_ts - self.start_ts
        return self._duration

    def start(self):
        self.start_ts = time()

    def end(self):
        self.end_ts = time()

    @property
    def start_dt(self):
        if self._start_dt is None and self.start_ts:
            self._start_dt = datetime.utcfromtimestamp(self.start_ts)
        return self._start_dt

    @property
    def end_dt(self):
        if self._end_dt is None and self.end_ts:
            self._end_dt = datetime.utcfromtimestamp(self.end_ts)
        return self._end_dt


class KeyMaker(object):
    def __init__(self, backup_id, plugin_name):
        self.backup_id = backup_id
        self.plugin_name = plugin_name
        self.template = 'backup_{backup_id}/{plugin_name}/data'
        self.template = self.template.format(backup_id=backup_id,
                                             plugin_name=plugin_name)

    def make_key(self, path):
        return '/'.join(self.template, path)
