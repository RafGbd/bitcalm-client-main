import os
import gzip
import time
import shutil
import filecmp
import unittest
from datetime import datetime
from cStringIO import StringIO

from boto.s3.key import Key

from bitcalm.plugins.compress import GZipPipe
from bitcalm.plugins import s3storage, fsbackup, backup as backup_plugin
from bitcalm.utils import pipechunks, Duration
from bitcalm.core.base import Core
from bitcalm.core.backup import BStat, Backup
from bitcalm.collections import FileDict
from bitcalm.core import jsonstorage
from bitcalm.core.const import MB, CONF_DIR
from bitcalm.core.events import BackupEvent
from bitcalm.core import tasks


def compare_files(f1, f2):
    return filecmp.cmp(f1, f2, shallow=False)


# Compression tests

class FileChunkCompress(unittest.TestCase):
    def setUp(self):
        self.path_src = '/tmp/rand.dat'
        self.path_gz = self.path_src + '.gz'
        self.path_dec = self.path_src + '.dec'
        if not os.path.exists(self.path_src):
            with open(self.path_src, 'wb') as f:
                f.write(os.urandom(10 << 10))
        self.source = open(self.path_src, 'rb')
        self.gzipfile = gzip.open(self.path_gz, mode='wb')

    def tearDown(self):
        self.source.close()
        for path in (self.path_gz, self.path_dec):
            os.remove(path)

    def test_chunk_compress(self):
        for chunk in pipechunks(self.source, chunk_size=3 << 10):
            shutil.copyfileobj(chunk, self.gzipfile)
        self.gzipfile.close()
        with open(self.path_dec, 'wb') as dst, \
                gzip.open(self.gzipfile.filename, mode='rb') as src:
            shutil.copyfileobj(src, dst)
        self.assertTrue(filecmp.cmp(self.source.name,
                                    self.path_dec,
                                    shallow=False))


class RandomCompress(unittest.TestCase):
    def get_source(self):
        return StringIO(os.urandom(10 << 10))

    def setUp(self):
        self.source = self.get_source()
        self.dst = StringIO()

    def tearDown(self):
        self.source.close()
        self.dst.close()

    def get_pipe(self):
        return GZipPipe(self.source, close_src=False)

    def prepare(self):
        self.source.seek(0)
        self.dst.truncate(0)

    def decompress(self, fileobj):
        fileobj.seek(0)
        return gzip.GzipFile(fileobj=fileobj, mode='rb')

    def test_gzippipe(self):
        self.prepare()
        gz = self.get_pipe()
        shutil.copyfileobj(gz, self.dst, 1024)
        gz.close()
        self.assertEqual(self.source.getvalue(),
                         self.decompress(self.dst).read())

    def test_gzippipe_pipechunks(self):
        self.prepare()
        gz = self.get_pipe()
        cs = pipechunks(gz, chunk_size=3 << 10)
        for c in cs:
            shutil.copyfileobj(c, self.dst, 1024)
        self.assertEqual(self.source.getvalue(),
                         self.decompress(self.dst).read())


class ZeroCompress(RandomCompress):
    def get_source(self):
        return StringIO('0' * (10 << 10))


class TextCompress(RandomCompress):
    def get_source(self):
        with open('./bitcalm/core/base.py', 'r') as f:
            return StringIO(f.read())


# S3 storage tests

def get_bucket():
    auth = jsonstorage.load(os.path.join(CONF_DIR, 's3storage.conf'))
    return s3storage.get_bucket(**auth)


class Upload(unittest.TestCase):
    SIZE = 10 * MB

    def setUp(self):
        self.core = Core()
        self.username = 'test_user13'  # TODO: get username from config
        self.path_src = '/tmp/rand.dat'
        self.path_download = self.path_src + '.dl'
        if not os.path.exists(self.path_src) \
                or os.stat(self.path_src).st_size != self.SIZE:
            with open(self.path_src, 'wb') as f:
                f.write(os.urandom(self.SIZE))
        self.source = open(self.path_src, 'rb')

    def tearDown(self):
        self.source.close()
        for path in (self.path_src, self.path_download):
            os.remove(path)

    def test_upload(self):
        key_name = self.username + '/test.dat'
        plugin = s3storage.S3Storage(self.core)
        plugin.upload(self.source, key_name)
        key = Key(get_bucket(), key_name)
        key.get_contents_to_filename(self.path_download)
        self.assertTrue(compare_files(self.path_src, self.path_download))


class MultipartUpload(Upload):
    SIZE = 100 * MB


# Backup tests

class BackupTest(unittest.TestCase):
    def setUp(self):
        self.core = Core()
        self.core.load_plugins()
        self.core.run()
        self.backup_plugin = tuple(self.core.listeners[BackupEvent])[0]

    def test_backup(self):
        self.backup_plugin.backup(None)


class Stat(unittest.TestCase):
    def setUp(self):
        self.status = BStat()
        self.items = dict(zip('abcd', xrange(4)))
        self.items['e'] = 'str'

    def test_lock(self):
        self.assertFalse(self.status._lock.locked())
        with self.status:
            self.assertTrue(self.status._lock.locked())
        self.assertFalse(self.status._lock.locked())

    def test_dict(self):
        for key, value in self.items.iteritems():
            self.status[key] = value
        self.assertDictEqual(self.status, self.items)

    def test_update(self):
        upd = self.status.last_update
        self.status['a'] = 2
        self.assertGreater(self.status.last_update, upd)

    def test_default(self):
        self.assertEqual(self.status['z'], self.status.default())


class BackupControllerTest(unittest.TestCase):
    def setUp(self):
        self.core = Core(config=None)

    def new_task(self):
        return tasks.BackupStatusTask()

    def test_backup_status(self):
        ctl = backup_plugin.BackupController(core=self.core)
        # Status is unknown
        task = self.new_task()
        ctl.process_task(task)
        valid = {'prev_backup': None, 'next_backup': None}
        self.assertDictEqual(task.result, valid)
        # Backup is scheduled
        valid['prev_backup'] = time.time()
        valid['next_backup'] = valid['prev_backup'] + 60
        ctl.data.update(valid)
        for key, value in valid.iteritems():
            valid[key] = datetime.utcfromtimestamp(value)
        task = self.new_task()
        ctl.process_task(task)
        self.assertDictEqual(task.result, valid)
        # Backup is in progress
        ctl.backup = Backup()
        ctl.backup.on_start()
        ctl.data['current_backup_start'] = ctl.backup.time.start_ts
        task = self.new_task()
        ctl.process_task(task)
        self.assertTrue('duration' in task.result)


# FS tests

class FSWalk(unittest.TestCase):
    def setUp(self):
        self.basedir = '/tmp/bitcalm_fstest'
        self.dirs = 3
        self.subdirs = 3
        self.files = 2
        os.mkdir(self.basedir)
        for path in self._get_dirpaths():
            os.mkdir(path)
            for x in xrange(1, self.files+1):
                with open(os.path.join(path, 'file%i' % x), 'wb') as f:
                    f.write(os.urandom(1 << 10))

    def tearDown(self):
        shutil.rmtree(self.basedir)

    def _get_dirpaths(self):
        for x in xrange(1, self.dirs+1):
            dirname = os.path.join(self.basedir, 'dir%i' % x)
            yield dirname
            for x in xrange(1, self.subdirs+1):
                yield os.path.join(dirname, 'subdir%i' % x)

    def test_duplicate(self):
        seen = set()
        for path in fsbackup.backupfiles([self.basedir]):
            self.assertFalse(path in seen)
            seen.add(path)

    def test_quantity(self):
        quantity = (self.dirs + self.dirs * self.subdirs) * self.files
        self.assertEqual(sum((1 for p in fsbackup.iterfiles(self.basedir))),
                         quantity)

    def test_skip(self):
        self.assertTrue(fsbackup.skip('/dev/stdout'))


# Utility tests
class DurationTest(unittest.TestCase):
    def setUp(self):
        self.start = time.time()
        self.duration = 5
        self.end = self.start + self.duration

    def get_object(self):
        return Duration()

    def test_duration(self):
        obj = self.get_object()
        self.assertEqual(obj.duration, 0)
        obj.start_ts = self.start
        self.assertTrue(isinstance(obj.duration, float))
        obj.end_ts = self.end
        self.assertEqual(obj.duration, self.duration)

    def test_datetime(self):
        obj = self.get_object()
        self.assertIsNone(obj.start_dt)
        self.assertIsNone(obj.end_dt)
        obj.start_ts = self.start
        self.assertEqual(obj.start_dt, datetime.utcfromtimestamp(self.start))
        obj.end_ts = self.end
        self.assertEqual(obj.end_dt, datetime.utcfromtimestamp(self.end))


# Data tests
class FileDictTest(unittest.TestCase):
    def setUp(self):
        self.path = '/tmp/test.json'

    def tearDown(self):
        self.del_file()

    def del_file(self):
        try:
            os.remove(self.path)
        except OSError:
            pass

    def get_object(self):
        return FileDict(self.path)

    def get_dict(self):
        return {s:n for n, s in enumerate('abcd')}

    def test_save(self):
        fdict = self.get_object()
        d = self.get_dict()
        fdict.update(d)
        fdict.save()
        self.assertDictEqual(d, jsonstorage.load(self.path))

    def test_load(self):
        fdict = self.get_object()
        self.del_file()
        fdict.load()
        d = self.get_dict()
        jsonstorage.save(self.path, d)
        fdict.load()
        self.assertDictEqual(d, fdict)

    def test_edit(self):
        d = self.get_dict()
        jsonstorage.save(self.path, d)
        fdict = self.get_object()
        fdict.load()
        fdict['e'] = 5
        fdict['a'] = 6
        fdict.load()
        self.assertDictEqual(d, fdict)


if __name__ == '__main__':
    unittest.main()
